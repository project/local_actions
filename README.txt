Local Actions

This is a small module that enables you to add one or more Local Action buttons to any Drupal page, without needing to write any hook code. Typical use case is a view that lists items of a particular content type, where it's nice to have a button that opens the page for creating a new item.

I was quite surprised to find that no such module existed, all my searches just led me to code snippets for adding hook_menu_local_tasks_alter(). Although it's simple code to add to a custom module, I felt a configurable feature would be useful.

Hence this Local Actions module! (If there is an existing module that I've missed, please let me know.)

To use, just install in the usual way, and you will find a new option "Local Actions" under Configuration->User Interface.

The main page lists all existing actions, with an Add button to create a new one.

To define an action, you need to enter a name (for administration only), the path of the host page on which the button is to be displayed, and the path of the page the action button will link to. The label is optional - if the linked page has a menu entry, the button label will be taken from the menu label. If the page has no menu entry, or you want to override the default, enter a label here. The weight is only required if you need to position the action button in relation to other buttons (pre-existing or also created by this module) on the same page. Some experiment may be required in relation to existing buttons.

Permissions of the linked page are respected, if the user does not have access to the page then no action button will be displayed.

This module is only for Drupal 7, I don't  run any sites under Drupal 8 (yet), so have no base on which to develop.
