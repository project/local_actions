<?php

/**
 * @file
 * Administration pages.
 */

/**
 * Form callback for main module admin page.
 * Lists all configured actions.
 */
function local_actions_list($form, &$form_state) {
  $form['local_actions_list'] = array(
    '#prefix' => '<table>',
    '#suffix' => '</table>',
    '#tree' => TRUE,
    '#weight' => '110',
  );

  $form['local_actions_list']['local_actions_list_header'] = array(
    '#markup' => '<thead>
      <tr>
        <th>'.t('Name').'</th>
        <th>'.t('Host path').'</th>
        <th>'.t('Link path').'</th>
        <th>'.t('Label').'</th>
        <th>'.t('Weight').'</th>
      </tr>
    </thead>',
  );

  $local_actions = db_select('local_actions', 'la')
    ->fields('la')
    ->orderBy('name')
    ->execute()
    ->fetchAll();

  $count = count($local_actions);

  for ($i = 0; $i < $count; $i++) {
    $form['local_actions_list']['row_' . $i] = array(
      '#prefix' => '<tr class="'.($i % 2 ? "odd" : "even").'">',
      '#suffix' => '</tr>',
    );
    $form['local_actions_list']['row_' . $i]['local_actions_list_name'] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#markup' => l($local_actions[$i]->name, 'admin/config/user-interface/local_actions/edit/'.$local_actions[$i]->id.''),
    );
    $form['local_actions_list']['row_' . $i]['local_actions_list_host_path'] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#markup' => $local_actions[$i]->host_path,
    );
    $form['local_actions_list']['row_' . $i]['local_actions_list_link_path'] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#markup' => $local_actions[$i]->link_path,
    );
    $form['local_actions_list']['row_' . $i]['local_actions_list_label'] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#markup' => $local_actions[$i]->label,
    );
    $form['local_actions_list']['row_' . $i]['local_actions_list_weight'] = array(
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#markup' => $local_actions[$i]->weight,
    );
  }
  return $form;
}

/**
 * Create or edit an action.
 *
 * @param int $id
 *  id of action record, or 0 to create a new action.
 *
 */
function local_actions_edit_form($form, $form_state, $id = 0) {
  drupal_set_title(t('!mode local action', array('!mode' => $id? 'Edit' : 'Add')));
  $local_action = $id?
    db_select('local_actions', 'la')
      ->fields('la')
      ->condition('id', $id, '=')
      ->execute()
      ->fetchAssoc()
  :
    array(
      'name' => '',
      'host_path' => '',
      'link_path' => '',
      'label' => '',
      'weight' => 0,
    )
  ;
  $form['local_actions_edit'] = array(
    'id' => array(
      '#type' => 'value',
      '#value' => $id,
    ),
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name of action'),
      '#description' => t('Administrative name, must be unique.'),
      '#required' => TRUE,
      '#default_value' => $local_action['name'],
    ),
    'host_path' => array(
      '#type' => 'textfield',
      '#title' => t('Host path'),
      '#description' => t('Path of host page.'),
      '#required' => TRUE,
      '#default_value' => $local_action['host_path'],
    ),
    'link_path' => array(
      '#type' => 'textfield',
      '#title' => t('Link path'),
      '#description' => t('Path of linked page.'),
      '#required' => TRUE,
      '#default_value' => $local_action['link_path'],
    ),
    'label' => array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#description' => t("Label for link, leave blank to use default from link's menu entry."),
      '#default_value' => $local_action['label'],
    ),
    'weight' => array(
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
      ),
      '#title' => t('Weight'),
      '#description' => t("Weight of link, for correct positioning."),
      '#required' => TRUE,
      '#default_value' => $local_action['weight'],
    ),
    'actions' => array(
      '#type' => 'actions',
      'submit' => array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#submit' => array('local_actions_form_submit'),
      ),
      'delete' => $id? array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('local_actions_form_delete'),
      ) : NULL,
    )
  );
  return $form;
}

/**
 * Submit function for edit form to save changes (create or update).
 */
function local_actions_form_submit($form, &$form_state) {
  $values = array(
    'name' => $form_state['values']['name'],
    'host_path' => $form_state['values']['host_path'],
    'link_path' => $form_state['values']['link_path'],
    'label' => $form_state['values']['label'],
    'weight' => $form_state['values']['weight'],
  );
  if ($id = $form_state['values']['id']) {
    db_update('local_actions')
      ->fields($values)
      ->condition('id', $id, '=')
      ->execute();
    drupal_set_message(t('Local action updated.'));
  }
  else {
    db_insert('local_actions')
      ->fields($values)
      ->execute();
    drupal_set_message(t('Local action created.'));
  }
  drupal_goto('admin/config/user-interface/local_actions');
}

/**
 * Submit function for edit form to delete a record.
 */
function local_actions_form_delete($form, &$form_state) {
  $form_state['redirect'] = '/admin/config/user-interface/local_actions/delete/' . $form_state['values']['id'];
}

/**
 * Form callback for delete confirmaation.
 */
function local_actions_delete_confirm($form, &$form_state, $id) {
  $local_action = db_select('local_actions', 'la')
    ->fields('la')
    ->condition('id', $id, '=')
    ->execute()
    ->fetchAssoc();
  $name = $local_action['name'];

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the local action %name?', array('%name' => $name)),
    'admin/config/user-interface/local_actions'
  );
}

/**
 * Submit function for delete confirmaation.
 */
function local_actions_delete_confirm_submit($form, &$form_state) {
  db_delete('local_actions')
    ->condition('id', $form_state['values']['id'], '=')
    ->execute();
  drupal_set_message(t('Local action %name deleted', array('%name' => $form_state['values']['name'])));
  drupal_goto('admin/config/user-interface/local_actions');
}
